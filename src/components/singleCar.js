import React from 'react';
import Col from 'react-bootstrap/Col';
import {
  MDBCard,
  MDBCardImage,
  MDBCardBody,
  MDBCardTitle,
  MDBCardText,
  MDBRow,
  MDBCol,
  MDBBtn,
  MDBCardHeader,
  MDBIcon,MDBCardLink
} from 'mdb-react-ui-kit';

import {useEffect, useState} from "react";

export default function SingleCar({carProp}) {

const {name, brand, location, description, userId,price,productImage,type} = carProp;
     let carType = "";
	if(userId=="host"){
      carType="Host";
  }else{
      carType="Affliates";
  }
  return (
     <>
      <Col md={3} className="mb-3">
      <MDBCol className="card-style mb-5">
       <MDBCard> 
        <div className="rent-item mb-4">
                        <img className="img-fluid mb-4" src={`${productImage}`}  alt="" />
                        <h4 className="text-uppercase mb-4">{name}</h4>
                        <div className="d-flex justify-content-center mb-4">
                            <div className="px-2">
                                <i className="fa fa-car text-primary mr-1"> </i>
                                <span>2015</span>
                            </div>
                            <div className="px-2 border-left border-right">
                                <i className="fa fa-cogs text-primary mr-1"> </i>
                                <span>AUTO</span>
                            </div>
                            <div className="px-2">
                                <i className="fa fa-road text-primary mr-1"> </i>
                                <span>25K</span>
                            </div>
                        </div>
                        <a className="btn btn-warning px-3" href="">$99.00/Day</a>
                    </div>
           </MDBCard>
           </MDBCol>
         {/*
          <MDBCol classname="card-style mb-5">
        <MDBCard> <MDBCardImage
            src={`${productImage}`} 
            alt='...'
            position='top'
          />
          <MDBCardBody>
            <MDBCardTitle>{name}</MDBCardTitle>
            <MDBCardText classname="text-wrap">Description:{description}
            </MDBCardText>
            <MDBCardText>
           
            <MDBCardLink href='#'>
            Type:{carType}</MDBCardLink>
         </MDBCardText>
             <MDBCardText>
           
            <MDBCardLink href='#'> <MDBIcon fas icon="dollar-sign" />{price}/day</MDBCardLink>
         </MDBCardText>
            <MDBBtn href='#'>Rent Now</MDBBtn>
            
          </MDBCardBody>
           </MDBCard>
      </MDBCol>*/}
       

</Col>
       
     
</>
  );
}