import '../App.css';
import React, { useState } from 'react';
import { Link } from "react-router-dom";
import {
  MDBNavbar,
  MDBContainer,
  MDBIcon,
  MDBNavbarNav,
  MDBNavbarItem,
  MDBNavbarLink,
  MDBNavbarToggler,
  MDBCollapse,
  MDBDropdown,
  MDBDropdownMenu,
  MDBDropdownToggle,
  MDBDropdownItem,
  MDBNavbarBrand
} from 'mdb-react-ui-kit';

export default function App() {
  const [showNavRight, setShowNavRight] = useState(false);
   const [showNavColor, setShowNavColor] = useState(false);
  const [showNavColorSecond, setShowNavColorSecond] = useState(false);
  const [showNavColorThird, setShowNavColorThird] = useState(false);
  return (
    <>
      <MDBNavbar expand='lg' dark bgColor='dark'  >
        <MDBContainer fluid>
          <MDBNavbarBrand href='/'>
            <div class="brand-header"><span class="blocktext">TORO</span></div>
           </MDBNavbarBrand>
           <MDBNavbarToggler
          type='button'
          data-target='#navbarRightAlignExample'
          aria-controls='navbarRightAlignExample'
          aria-expanded='false'
          aria-label='Toggle navigation'
          onClick={() => setShowNavRight(!showNavRight)}
        >
          <MDBIcon icon='bars' fas />
        </MDBNavbarToggler>

        <MDBCollapse navbar show={showNavRight}>
          <MDBNavbarNav right fullWidth={false} className='mb-2 mb-lg-0'>
            <MDBNavbarItem>
              <MDBNavbarLink href="/" className="nav-style fw-bolder text-light">Home</MDBNavbarLink>
               
            </MDBNavbarItem>
            <MDBNavbarItem >
              <MDBNavbarLink href="/carlist" className="nav-style fw-bolder text-light">
                All Vehicles
              </MDBNavbarLink>
            </MDBNavbarItem>
            <MDBNavbarItem>
              <MDBNavbarLink href="/hostcar" className="nav-style fw-bolder text-light">Host</MDBNavbarLink>
               
            </MDBNavbarItem>
            <MDBNavbarItem>
            <MDBNavbarLink href="/affiliates" className="nav-style fw-bolder text-light">Affiliates</MDBNavbarLink>
             </MDBNavbarItem>
          </MDBNavbarNav>
        </MDBCollapse>
        </MDBContainer>
      </MDBNavbar>
    </>
  );
}