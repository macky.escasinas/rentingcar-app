import React from 'react';
import {useEffect, useState} from "react";
import Form from 'react-bootstrap/Form';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import {
  MDBBtn,
  MDBRow,
  MDBCol,MDBContainer,
    MDBCard,
  MDBCardBody,
  MDBCardTitle,
  MDBCardText,MDBIcon,MDBCardImage,
  MDBCarousel,
  MDBCarouselItem,
} from 'mdb-react-ui-kit';
import AppNavbar from "../components/AppNavbar";

import SingleCar from "../components/singleCar";
export default function Home() {
  const [cars, setCars] = useState([]);
   useEffect(() =>{
     fetch(`https://renting-api.onrender.com/cars/viewall-active-car`)
        .then(res => res.json())
        .then(data => {
            setCars(data.map(car =>{
                return(
                    <SingleCar key={car._id} carProp={car} />
                )
            }))
        })
    },[])

  return (
    <>

    <AppNavbar />

    <div className="carousel-container">
      <MDBCarousel showIndicators showControls fade>
      <MDBCarouselItem
        className='w-100 d-block'
        itemId={1}
        src='/carousel-1.jpg'
        alt='...'
      > 
                    <div className="carousel-caption d-flex flex-column align-items-center justify-content-center">
                        <div className="p-3" >
                            <h4 className="text-white text-uppercase mb-md-3">Find your drive</h4>
                            <h1 className="display-1 text-white mb-md-4">Explore the world's largest car sharing marketplace</h1>
                            <a href="" className="btn btn-warning py-md-3 px-md-5 mt-2 warning">Book Now</a>
                        </div>
                    </div>
              

      </MDBCarouselItem>

      <MDBCarouselItem
        className='w-100 d-block'
        itemId={2}
        src='/carousel-2.jpg'
        alt='...'
      >
        <div className="carousel-caption d-flex flex-column align-items-center justify-content-center">
                        <div className="p-3" >
                            <h4 className="text-white text-uppercase mb-md-3">Plan Your Trip</h4>
                            <h2 className="display-1 text-white mb-md-4">Save Big with <br/>our car rental</h2>
                            <a href="" className="btn btn-warning py-md-3 px-md-5 mt-2 ">Book Now</a>
                        </div>
                    </div>
      </MDBCarouselItem>
    </MDBCarousel>
  </div>
  <br/>

<MDBContainer fluid className="search-container">
     <h1 class="display-4 text-uppercase text-center mb-3">Fuel your daydreams</h1>
     <div class="container-fluid py-5">
         <div class="container py-5">
             <div class="row mx-0">
                 <div class="col-lg-6 px-0">
                     <div class="px-5 bg-secondary d-flex align-items-center justify-content-between container-banner" >
                         <img class="img-fluid flex-shrink-0 ml-n5 w-50 mr-4" src="/banner-left.png" alt=""/>
                         <div class="text-right">
                             <h2 class="text-uppercase text-light mb-3">Want to be a host?</h2>
         
                             <a class="btn btn-warning py-2 px-4" href="/co-host">Become a host</a>
                         </div>
                     </div>
                 </div>
                 <div class="col-lg-6 px-0">
                     <div class="px-5 bg-dark d-flex align-items-center justify-content-between container-banner" >
                         <div class="text-left">
                             <h2 class="text-uppercase text-light mb-3">Looking for a car?</h2>

                             <a class="btn btn-warning py-2 px-4" href="#book_now">Book Now</a>
                         </div>
                         <img class="img-fluid flex-shrink-0 mr-n5 w-50 ml-4" src="/banner-right.png" alt=""/>
                     </div>
                 </div>
             </div>
         </div>
     </div>
    </MDBContainer>


 <MDBContainer fluid id="book_now" >
      <h1 class="display-4 text-uppercase text-center mb-3">Find Your Car</h1>
     <div className="container-fluid bg-white pt-3 px-lg-5">
         <div className="row mx-n2">
             <div className="col-xl-2 col-lg-4 col-md-6 px-2">
                
                  <Form.Select aria-label="Default select example" className="search-style">
      <option>Pickup Location</option>
      <option value="1">One</option>
      <option value="2">Two</option>
      <option value="3">Three</option>
    </Form.Select>
             </div>
             <div className="col-xl-2 col-lg-4 col-md-6 px-2">
                
                 <Form.Select aria-label="Default select example" className="search-style">
      <option>Drop Location</option>
      <option value="1">One</option>
      <option value="2">Two</option>
      <option value="3">Three</option>
    </Form.Select>
             </div>
             <div className="col-xl-2 col-lg-4 col-md-6 px-2">
                 <div className="date mb-3" id="date" data-target-input="nearest">
                     <input type="text" className="form-control p-4 datetimepicker-input search-style" placeholder="Pickup Date"
                         data-target="#date" data-toggle="datetimepicker" />
                 </div>
             </div>
             <div class="col-xl-2 col-lg-4 col-md-6 px-2">
                 <div className="time mb-3" id="time" data-target-input="nearest">
                     <input type="text" className="form-control p-4 datetimepicker-input search-style" placeholder="Pickup Time"
                         data-target="#time" data-toggle="datetimepicker" />
                 </div>
             </div>
             <div class="col-xl-2 col-lg-4 col-md-6 px-2">
               
                  <Form.Select aria-label="Default select example" className="search-style">
      <option>Select A Car</option>
      <option value="1">One</option>
      <option value="2">Two</option>
      <option value="3">Three</option>
    </Form.Select>
             </div>

             <div className="col-xl-2 col-lg-4 col-md-6 px-2">
                 <button className="btn btn-primary btn-block mb-3 search-style search-style"  type="submit" >Search</button>
             </div>
         </div>
     </div>
   </MDBContainer>

   
     <Container>
      <Row >
       

        {cars}
      </Row>
     

    </Container>

   {/* <div class="container-fluid py-5">
        <div class="container pt-5 pb-3">
            <h1 class="display-1 text-primary text-center">03</h1>
            <h1 class="display-4 text-uppercase text-center mb-5">Find Your Car</h1>
            <div class="row">
                <div class="col-lg-4 col-md-6 mb-2">
                    <div class="rent-item mb-4">
                        <img class="img-fluid mb-4" src="/car-rent-1.png" alt="" />
                        <h4 class="text-uppercase mb-4">Mercedes Benz R3</h4>
                        <div class="d-flex justify-content-center tex-center mb-4">
                            <div class="px-2">
                                <i class="fa fa-car text-primary mr-1"></i>
                                <span>2015</span>
                            </div>
                            <div class="px-2 border-left border-right">
                                <i class="fa fa-cogs text-primary mr-1"></i>
                                <span>AUTO</span>
                            </div>
                            <div class="px-2">
                                <i class="fa fa-road text-primary mr-1"></i>
                                <span>25K</span>
                            </div>
                        </div>
                        <a class="btn btn-warning px-3" href="">$99.00/Day</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 mb-2">
                    <div class="rent-item active mb-4">
                        <img class="img-fluid mb-4" src="/car-rent-2.png" alt="" />
                        <h4 class="text-uppercase mb-4">Mercedes Benz R3</h4>
                        <div class="d-flex justify-content-center mb-4">
                            <div class="px-2">
                                <i class="fa fa-car text-primary mr-1"></i>
                                <span>2015</span>
                            </div>
                            <div class="px-2 border-left border-right">
                                <i class="fa fa-cogs text-primary mr-1"></i>
                                <span>AUTO</span>
                            </div>
                            <div class="px-2">
                                <i class="fa fa-road text-primary mr-1"></i>
                                <span>25K</span>
                            </div>
                        </div>
                        <a class="btn btn-warning px-3" href="">$99.00/Day</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 mb-2">
                    <div class="rent-item mb-4">
                        <img class="img-fluid mb-4" src="/car-rent-3.png" alt="" />
                        <h4 class="text-uppercase mb-4">Mercedes Benz R3</h4>
                        <div class="d-flex justify-content-center mb-4">
                            <div class="px-2">
                                <i class="fa fa-car text-primary mr-1"></i>
                                <span>2015</span>
                            </div>
                            <div class="px-2 border-left border-right">
                                <i class="fa fa-cogs text-primary mr-1"></i>
                                <span>AUTO</span>
                            </div>
                            <div class="px-2">
                                <i class="fa fa-road text-primary mr-1"></i>
                                <span>25K</span>
                            </div>
                        </div>
                        <a class="btn btn-warning px-3" href="">$99.00/Day</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 mb-2">
                    <div class="rent-item mb-4">
                        <img class="img-fluid mb-4" src="/car-rent-4.png" alt="" />
                        <h4 class="text-uppercase mb-4">Mercedes Benz R3</h4>
                        <div class="d-flex justify-content-center mb-4">
                            <div class="px-2">
                                <i class="fa fa-car text-primary mr-1"></i>
                                <span>2015</span>
                            </div>
                            <div class="px-2 border-left border-right">
                                <i class="fa fa-cogs text-primary mr-1"></i>
                                <span>AUTO</span>
                            </div>
                            <div class="px-2">
                                <i class="fa fa-road text-primary mr-1"></i>
                                <span>25K</span>
                            </div>
                        </div>
                        <a class="btn btn-warning px-3" href="">$99.00/Day</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 mb-2">
                    <div class="rent-item mb-4">
                        <img class="img-fluid mb-4" src="/car-rent-5.png" alt="" />
                        <h4 class="text-uppercase mb-4">Mercedes Benz R3</h4>
                        <div class="d-flex justify-content-center mb-4">
                            <div class="px-2">
                                <i class="fa fa-car text-primary mr-1"></i>
                                <span>2015</span>
                            </div>
                            <div class="px-2 border-left border-right">
                                <i class="fa fa-cogs text-primary mr-1"></i>
                                <span>AUTO</span>
                            </div>
                            <div class="px-2">
                                <i class="fa fa-road text-primary mr-1"></i>
                                <span>25K</span>
                            </div>
                        </div>
                        <a class="btn btn-warning px-3" href="">$99.00/Day</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 mb-2">
                    <div class="rent-item mb-4">
                        <img class="img-fluid mb-4" src="/car-rent-6.png" alt="" />
                        <h4 class="text-uppercase mb-4">Mercedes Benz R3</h4>
                        <div class="d-flex justify-content-center mb-4">
                            <div class="px-2">
                                <i class="fa fa-car text-primary mr-1"></i>
                                <span>2015</span>
                            </div>
                            <div class="px-2 border-left border-right">
                                <i class="fa fa-cogs text-primary mr-1"></i>
                                <span>AUTO</span>
                            </div>
                            <div class="px-2">
                                <i class="fa fa-road text-primary mr-1"></i>
                                <span>25K</span>
                            </div>
                        </div>
                        <a class="btn btn-warning px-3" href="">$99.00/Day</a>
                    </div>
                </div>
            </div>
        </div>
    </div>*/}
    </>
  );
}