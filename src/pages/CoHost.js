import React from 'react';
import {useEffect, useState} from "react";

import {
  MDBBtn,
  MDBRow,
  MDBCol,MDBContainer,
    MDBCard,
  MDBCardBody,
  MDBCardTitle,
  MDBCardText,MDBIcon,MDBCardImage
} from 'mdb-react-ui-kit';
import AppNavbar from "../components/AppNavbar";
export default function Cohost() {



  return (
    <>

    <div className="bg-component">
       <AppNavbar />

 <div
        id='intro-example'
        className='p-5 text-center bg-image'
        
      >
        <div className='mask'>
          <div className='d-flex align-items-center h-100'>
            <div className='text-light'>
              <h1 className='p-5'>You want to start a car <br/>sharing business on Turo?</h1>
              <h5 className='mb-4'>YOU'VE COME TO THE RIGHT PLACE.</h5>
              <MDBBtn
                className="m-2 text-light"
                tag="a"
                outline
                size="lg"
                rel="nofollow"
                target="_blank"
                href='#'
              >
                  Learn More
              </MDBBtn>
             
            </div>
          </div>
        </div>
      </div>



<div className="container-background text-center">
     <h1><b>
WHY CHOOSE US</b></h1>
  <img src='./cars.png' className='img-fluid' alt='...' />
 <div className="p-2 g-col-3" ><p>Take control of your financial future while cultivating your entrepreneurial fire with Turo, the world’s largest car sharing marketplace.

Turo gives budding entrepreneurs the tools and resources they need to build a small, successful portfolio of cars to share on the marketplace, and the opportunity to add thousands to their annual income.

List your first car now to get started, then build your business plan and scale how you want!</p></div>
<h2>Build a business that’s...  </h2>
<div className="d-flex flex-sm-column flex-lg-row justify-content-center p-2 text-center ">
  
    <div className="p-2 g-col-4" >
    <MDBCol>
        <MDBCard alignment='center'>

             <MDBCardBody>
             <MDBIcon fas icon="balance-scale" size='3x' />
               <MDBCardTitle><b>Scalable</b></MDBCardTitle>
               <MDBCardText>You choose how many cars to share, scaling your business up or down however you want, and whether to reinvest your earnings or cash out.</MDBCardText>
             </MDBCardBody>
            
           </MDBCard>
    </MDBCol>
    </div>

    <div className="p-2 g-col-4" >
    <MDBCol>
        <MDBCard alignment='center'>

             <MDBCardBody><MDBIcon fas icon="universal-access" size='3x' />
               <MDBCardTitle><b>Accessible</b></MDBCardTitle>
               <MDBCardText>Start with a car you already own or buy one to share — any car owner can start exercising their entrepreneurial muscles.</MDBCardText>
             </MDBCardBody>
            
           </MDBCard>
    </MDBCol>
    </div>
    <div className="p-2 g-col-4" >
    <MDBCol>
        <MDBCard alignment='center'>

             <MDBCardBody>
             <MDBIcon fas icon="adjust" size='3x' />
               <MDBCardTitle><b>Flexible</b></MDBCardTitle>
               <MDBCardText>Whether you want to commit a lot of time or a little, you can earn at home or on the go, on your schedule, and divest any time.</MDBCardText>
             </MDBCardBody>
            
           </MDBCard>
    </MDBCol>
    </div>
       </div>
<div className="bg-build-image" >
<br/><h1 className="text-white">Built-in infrastructure to get you up & running</h1>
 <img src='./banner-left.png' className='img-fluid bg-build-card' alt='...' />
  
  
  <img src='./banner-right.png' className='img-fluid bg-build-card-right' alt='...' />
     <MDBRow className="justify-content-center">
        <MDBCol size='3' sm="12" lg='6' className="p-2 g-col-3" >
          <MDBCard>
                <MDBCardBody>
                  <MDBCardTitle>Demand generation</MDBCardTitle>
                  <MDBCardText>Get instant access to a prescreened customer base of over 14 million worldwide, plus marketing and advertising support for your cars, funded by Turo, the world’s leading car sharing marketplace.</MDBCardText>
                 
                </MDBCardBody>
              </MDBCard>
        </MDBCol>
        <MDBCol size='3' sm="12" lg='6' className="p-2 g-col-3">
          <MDBCard>
        
                <MDBCardBody>
                  <MDBCardTitle>An easy-to-use app</MDBCardTitle>
                  <MDBCardText>Manage your business and your bookings seamlessly on the go — accept trips, tweak your pricing, message your guests, and more, all from your phone.</MDBCardText>
                  
                </MDBCardBody>
              </MDBCard>
        </MDBCol>
      
      </MDBRow> 
      <MDBRow className="justify-content-center " >
      <MDBCol size='3' sm="12" lg='6' className="p-2 g-col-3">
          <MDBCard>
        
                <MDBCardBody>
                
                  <MDBCardTitle>Insurance included</MDBCardTitle>
                  <MDBCardText>Rest easy knowing you’re covered with $750,000 in liability insurance from Travelers, plus you choose from an array of protection plans that include varying levels of reimbursement for physical damage.</MDBCardText>
       
                </MDBCardBody>
              </MDBCard>
        </MDBCol>
        <MDBCol size='3' sm="12" lg='6' className="p-2 g-col-4" >
          <MDBCard>
                <MDBCardBody>
                  <MDBCardTitle>Safety & support</MDBCardTitle>
                  <MDBCardText>Get access to 24/7 customer support, roadside assistance for your guests, an experienced trust and safety team to support you through thick and thin, and one-on-one business coaching to share market intelligence and help you build a savvy business.</MDBCardText>
       
                </MDBCardBody>
              </MDBCard>
        </MDBCol>

     
      </MDBRow>

     
</div>


      
  </div>
  
  </div>

    </>
  );
}