
import {useState, useEffect} from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

import {

  MDBContainer
} from 'mdb-react-ui-kit';


import AppNavbar from "./components/AppNavbar";
import Home from "./pages/Home";
import CarList from "./pages/CarList";

import HostCar from "./pages/HostCar";
import AffiliatesCar from "./pages/AffiliatesCar";
import CoHost from "./pages/CoHost";

function App() {
  return (
   <div>
     <Router>
       
          < MDBContainer fluid>
              
              <Routes>
                <Route exact path ="/" element={<Home />} />
                 <Route exact path ="/affiliates" element={<AffiliatesCar />} />
                  <Route exact path ="/carlist" element={<CarList />} />
                   <Route exact path ="/hostcar" element={<HostCar />} />
                   <Route exact path ="/co-host" element={<CoHost />} />
                  
             
              </Routes>
          </ MDBContainer>
        </Router>

   </div>
  );
}

export default App;
